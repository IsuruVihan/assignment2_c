/* Isuru Harischandra
 * Student ID :- 202032
 */
 
// This program will get an integer as an user input tells whether it's odd or even

#include <stdio.h>

int main() {

    int number;

    // get the user input
    printf("Enter a number :");
    scanf("%d", &number);

    if(number % 2 == 0) { // even
        printf("Your number is even!");
    } else { // odd
        printf("Your number is odd!");
    }

    return 0;

}
