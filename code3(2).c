/* Isuru Harischandra
 * Student ID :- 202032
 */
 
// This program will get a character and tells whether it's a vowel, consonant or not an alphabat character

#include <stdio.h>

int main() {

    int character;

    // get the user input
    printf("Enter a character :");
    character = getchar();

    if ((character <= 122 && character >= 97) || (character <= 90 && character >= 65)) { // alphabet characters
        switch (character) {
            case 65 :
            case 69 :
            case 73 :
            case 79 :
            case 85 :
            case 97 :
            case 101 :
            case 105 :
            case 111 :
            case 117 :  // vowels
                printf("It's a Vowel!"); 
                break;
            default: // consonant
                printf("It's a Consonant!"); 
        }
    } else { // other characters
        printf("It's not an alphabet character!");
    }

    return 0;

}