/* Isuru Harischandra
 * Student ID :- 202032
 */
 
// This program gets an integer as user input and tell whether iit is zero, negative or positive

#include <stdio.h>

int main() {

    int number;

    // get the user input
    printf("Enter a number :");
    scanf("%d", &number);
    
    if(number < 0) { // negative
        printf("You entered a negative number!");
    } else if(number > 0) { // positive
        printf("You entered a positive number!");
    } else { // zero
        printf("You entered zero!");
    }

    return 0;

}